from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class ProjectPage(models.Model):
    owned_to_project = models.ForeignKey(Project, on_delete=models.CASCADE)
    title = models.CharField(max_length=64)
    text = models.TextField()
    parent_page = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return 'Проект "{0}". Страница "{1}"'.format(self.owned_to_project.__str__(),
                                                     self.title)


class Privilege(models.Model):
    rights = models.CharField(max_length=32)

    def __str__(self):
        return self.rights


class UserPermission(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    project = models.ForeignKey(Project, models.CASCADE)
    privileges = models.ForeignKey(Privilege, models.CASCADE)

    def __str__(self):
        return '{0} имеет права "{1}" на проекте: {2}'.format(self.user.__str__(),
                                                              self.privileges.__str__(),
                                                              self.project.__str__())
