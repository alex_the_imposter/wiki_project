from django.contrib import admin
from .models import Project, ProjectPage, UserPermission, Privilege


admin.site.register(UserPermission)
admin.site.register(Privilege)
admin.site.register(Project)
admin.site.register(ProjectPage)
